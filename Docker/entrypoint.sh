#!/bin/bash

if [ ! -f "vendor/autoload.php" ]; then
    composer install --no-progress  --no-interaction
fi

if [ ! -f ".env" ]; then
    cp .env.example .env
fi

role=${CONTAINER_ROLE:-app}

if [ "$role" = "app" ]; then
    php artisan migrate --seed
    php artisan key:generate
    php artisan optimize:clear
    php artisan serve --port=$PORT --host=0.0.0.0 --env=.env
    exec docker-php-entrypoint "$@"

elif [ "$role" = "queue" ]; then
    php /var/www/artisan queue:work --verbose --tries=3 --timeout=180
fi



